﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

[Serializable]
public class PIDController
{
    [HideLabel, ReadOnly] public PIDControllerSettings constants;
    [SerializeField, ReadOnly] private float prevError;
    [SerializeField, ReadOnly] private float integral;

    public PIDController(PIDControllerSettings c)
    {
        this.constants = c;
    }

    public float Update(float error, float dt)
    {
        float derivative = (error - prevError) / dt;
        prevError = error;
        integral += error;
        float result = Calculate(error, derivative, integral);
        float clamped = Mathf.Clamp(result, -1, 1f);

        var preventIntegralWindup = !Mathf.Approximately(clamped, result)
                                    && Mathf.Approximately(Mathf.Sign(error), Mathf.Sign(result));
        if (preventIntegralWindup)
        {
            integral -= error;
            return Mathf.Clamp(Calculate(error, derivative, integral), -1, 1f);
        }

        return clamped;
    }

    private float Calculate(float error, float derivative, float integral)
    {
        return error * constants.ProportionalGain
               + integral * constants.IntegralGain
               + derivative * constants.DerivativeGain;
    }
}