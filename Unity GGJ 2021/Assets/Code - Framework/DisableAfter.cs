﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableAfter : MonoBehaviour
{
    public float DisableAfterSeconds = 10.0f;

    void Start()
    {
        StartCoroutine(Disable());
    }

    void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(Disable());
    }


    private IEnumerator Disable()
    {
        yield return new WaitForSeconds(DisableAfterSeconds);
        gameObject.SetActive(false);
    }
}