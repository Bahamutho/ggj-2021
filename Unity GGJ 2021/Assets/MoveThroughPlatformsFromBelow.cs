using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveThroughPlatformsFromBelow : MonoBehaviour
{
    private Rigidbody2D rb;

    private int platformsLayer = SettingsHolder.Instance.settings.PlatformsLayerMask.value;
    private int playerLayer => SettingsHolder.Instance.settings.PlayerLayerMask.value;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (rb.velocity.y > SettingsHolder.Instance.settings.GroundedUpwardVelocityTreshold)
        {
            Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("FloatyFloors"), true);
        }
        //else the collision will not be ignored
        else
        {
            Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("FloatyFloors"), false);
        }
    }
}