using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class WallCollisionDetectors : MonoBehaviour
{
    [OnValueChanged("calculatePositions")] public int SphereAmount = 5;
    [OnValueChanged("calculatePositions")] public float distanceBetweenSpheres = 0.1f;
    [OnValueChanged("calculatePositions")] public float radius = 0.1f;
    public LayerMask LayerMask;
    public Vector2 Offset = Vector2.zero;
    public int HighEmptySpace = 0;
    public int LowEmptySpace = 0;

    [ShowInInspector] private List<PositionAndCollision> relativePositions;

    void Start()
    {
        calculatePositions();
    }

    private void FixedUpdate()
    {
        HighEmptySpace = 0;
        LowEmptySpace = 0;

        for (var i = 0; i < relativePositions.Count; i++)
        {
            var positionAndCollision = relativePositions[i];
            var pos = GetPosition(positionAndCollision);
            var raycastHit2D = Physics2D.OverlapCircle(pos, radius, LayerMask.value);
            var collision = raycastHit2D != null;
            positionAndCollision.Collision = collision;
            if (i == 0 || collision)
                continue;
            if (i % 2 == 1)
            {
                HighEmptySpace++;
            }
            else
            {
                LowEmptySpace++;
            }
        }
    }

    public bool BelowLedge()
    {
        return HighEmptySpace > LowEmptySpace && HighEmptySpace > 0 && relativePositions[0].Collision;
    }

    private Vector2 GetPosition(PositionAndCollision pos)
    {
        return pos.Pos + (Vector2) transform.position + Offset;
    }

    [Button]
    private void calculatePositions()
    {
        var result = new List<PositionAndCollision>();

        for (int i = 0; i < SphereAmount; i++)
        {
            var even = i % 2 == 0;
            var y = Mathf.CeilToInt(i / 2f) * (distanceBetweenSpheres + radius * 2) * (even ? -1 : 1);
            var pos = new Vector2(0, y);
            result.Add(new PositionAndCollision() {Pos = pos, Collision = false});
        }

        relativePositions = result;
    }
    
    
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        for (var i = 0; i < relativePositions.Count; i++)
        {
            var pos = relativePositions[i];
            Gizmos.color = pos.Collision ? Color.green : Color.red;
            var position = GetPosition(pos);
            Gizmos.DrawWireSphere(position, radius);
            Handles.Label(position, $"{i}");
        }
    }
#endif
}

[Serializable, InlineProperty()]
public class PositionAndCollision
{
    [HorizontalGroup()] public Vector2 Pos;
    [HorizontalGroup()] [HideLabel] public bool Collision;
}