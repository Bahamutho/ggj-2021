using System;

using UnityEngine;

using TMPro;

class ItemCounter : MonoBehaviour
{
    public string textPrefix = "Items: ";
    protected TextMeshProUGUI text;
    protected InventoryController inventoryController;

    protected int count = 0;

    protected void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();

        GameObject player = GameObject.FindWithTag("Player");
        if (player)
        {
            inventoryController = player.GetComponent<InventoryController>();
        }
    }

    protected void OnEnable()
    {
        inventoryController.onEnqueue.AddListener(Increment);
        inventoryController.onDequeue.AddListener(Decrement);
    }

    protected void OnDisable()
    {
        inventoryController.onEnqueue.RemoveListener(Increment);
        inventoryController.onDequeue.RemoveListener(Decrement);
    }

    protected void Increment(GameObject gameObject)
    {
        count++;
        text.text = textPrefix + count;
    }

    protected void Decrement(GameObject gameObject)
    {
        count--;
        text.text = textPrefix + count;
    }
}
