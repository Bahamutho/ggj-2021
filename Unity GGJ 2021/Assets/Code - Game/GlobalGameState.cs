using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class GlobalGameState : SingletonMonoBehavior<GlobalGameState>
{
    [ShowInInspector, ReadOnly] private GlobalGameStateEnum gameState = GlobalGameStateEnum.Intermezzo;

    public AudioManager audioManager;
    public GlobalGameStateEnum GameState => gameState;
    private GameSettings settings => SettingsHolder.instance.settings;

    private void Start()
    {
        ChangeGameState(GlobalGameStateEnum.Home);
    }

    public void ChangeGameState(GlobalGameStateEnum newState)
    {
        if (newState == gameState)
            return;
        Debug.Log($"Switching from {gameState} to {newState}");
        gameState = newState;

        switch (newState)
        {
            case GlobalGameStateEnum.Adventure:
                audioManager.PlayClipsLoopWithIntro(settings.AdventureIntro, settings.AdventureLooping);
                return;
            case GlobalGameStateEnum.Danger:
                audioManager.PlayClipsLoopWithIntro(settings.DangerIntro, settings.DangerLooping);
                return;
            case GlobalGameStateEnum.Home:
                audioManager.PlayClipsLoopWithIntro(settings.LoungeIntro, settings.LoungeLooping);
                return;
            case GlobalGameStateEnum.Intermezzo:
                audioManager.PlayClipsLoopWithIntro(settings.Intermezzo, null);
                return;
        }

    }


}

public enum GlobalGameStateEnum
{
    Home,
    Adventure,
    Danger,
    Intermezzo
}