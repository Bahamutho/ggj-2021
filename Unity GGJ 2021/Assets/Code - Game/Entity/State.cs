using UnityEngine;
using UnityEngine.Events;

namespace Entity
{
    class State : SignalEmitter
    {
        public virtual bool Can() {
            return false;
        }
    }
}