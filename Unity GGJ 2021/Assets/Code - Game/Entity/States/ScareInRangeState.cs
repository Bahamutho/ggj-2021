using System;

using UnityEngine;

namespace Entity.States
{
    class ScareInRangeState : State
    {
        [SerializeField]
        protected RangeHandler rangeHandler;
        [SerializeField]
        protected SignalEmitter signalEmitter;

        public override bool Can()
        {
            return rangeHandler.GetCount() > 0;
        }

        protected new void OnEnable()
        {
            base.OnEnable();

            if (signalEmitter)
            {
                signalEmitter.enabled = true;
            }
        }

        protected new void OnDisable()
        {
            base.OnDisable();

            if (signalEmitter)
            {
                signalEmitter.enabled = false;
            }
        }
    }
}