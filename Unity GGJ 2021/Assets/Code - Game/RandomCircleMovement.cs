using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomCircleMovement : MonoBehaviour
{
    public float MinX = -8f, MaxX = 8f, MinY = -5f, MaxY = 5f;
    public double randomIntervalInSeconds = 1.5f;
    private Transform tr;
    private double lastTimeStep;
    
    void Start()
    {
        tr = GetComponent<Transform>();
        lastTimeStep = Time.timeSinceLevelLoadAsDouble;
    }

    void Update()
    {
        if (Time.timeSinceLevelLoadAsDouble > lastTimeStep + randomIntervalInSeconds)
        {
            lastTimeStep = Time.timeSinceLevelLoadAsDouble;
            tr.position = new Vector3(Random.Range(MinX, MaxX), Random.Range(MinY, MaxY));
        }
    }
}
