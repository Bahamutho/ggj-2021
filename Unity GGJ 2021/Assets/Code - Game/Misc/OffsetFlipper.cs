using System;

using UnityEngine;

class OffsetFlipper : MonoBehaviour
{
    [SerializeField]
    protected Direction direction = Direction.Left;

    public void Set(Direction directionNew)
    {
        if (direction == directionNew)
        {
            return;
        }

        direction = directionNew;

        // Toggle offset.
        transform.localPosition = new Vector3(-transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
    }

    public void Left()
    {
        Set(Direction.Left);
    }

    public void Right()
    {
        Set(Direction.Right);
    }
}