using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
class RangeHandler : MonoBehaviour
{
    protected new Collider2D collider2D;
    [SerializeField]
    protected List<Collider2D> items;

    public int mask = -1;
    public UnityEvent<Collider2D> onEnter;
    public UnityEvent<Collider2D> onExit;

    protected void Awake()
    {
        collider2D = GetComponent<Collider2D>();
        items = new List<Collider2D>();
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other.isTrigger || items.Contains(other))
        {
            return;
        }

        if (mask >= 0 && other.gameObject.layer != 0 && (mask | other.gameObject.layer) == 0) {
            return;
        }

        items.Add(other);
        onEnter.Invoke(other);
    }

    protected void OnTriggerExit2D(Collider2D other)
    {
        if (!items.Contains(other))
        {
            return;
        }

        items.Remove(other);
        onExit.Invoke(other);
    }

    public int GetCount()
    {
        return items.Count;
    }

    public Collider2D[] GetItems()
    {
        return items.ToArray();
    }

    public Collider2D GetNearest()
    {
        if (items.Count < 1)
        {
            return null;
        }
        if (items.Count == 1)
        {
            return items[0];
        }

        Collider2D nearest = null;
        float distance = float.MaxValue;
        ColliderDistance2D colliderDistance;
        foreach (Collider2D item in items)
        {
            colliderDistance = item.Distance(collider2D);
            if (colliderDistance.distance < distance)
            {
                distance = colliderDistance.distance;
                nearest = item;
            }
        }

        return nearest;
    }
}
