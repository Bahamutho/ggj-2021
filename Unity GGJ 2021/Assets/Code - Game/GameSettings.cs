﻿using DefaultNamespace;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu]
public class GameSettings : ScriptableObject
{
    public float YDeadZone = 0.1f;
    public float XDeadZone = 0.1f;
    public LayerMask HomeColliderLayerMask;
    public float ScaredJumpY = 3.0f;
    public float ScaredJumpX = 1.0f;
    public StateGuard FlipDirectionGuard;

    [Header("PlayerMovement")]
    public StateGuard MovementGuard;
    public StateGuard WalkingGuard;
    public float WalkingMaxSpeed = 5f;
    public float WalkingSensingThreshold = 0.35f;
    public float TimeToWalkingMaxSpeed = 1.0f;
    public float TimeToWalkingDeAccel = 0.5f;
    public LayerMask GroundedColliderLayerMask;
    public float GroundedMovementTreshold = 0.1f;
    public float GroundedUpwardVelocityTreshold = 0.5f;

    [Header("Jump Settings")]
    public int JumpTokenAmount = 1;

    public StateGuard FallingGuard;
    public StateGuard JumpGuard;
    public StateGuard WallSlideJumpGuard;

    public float TimeToMaxJumpApex = 1.0f;
    public float TimeToMinJumpApex = 0.5f;
    public float TimeToFall = 0.5f;
    public float MaxJumpHeight = 5.0f;
    public float MinJumpHeight = 1.0f;
    public float MinJumpSlowDownGravityScale = 35f;
    public float CoyoteTime = 0.3f;
    public float JumpQueueTime = 0.3f;
    public float WalkingGravity = 1.0f;

    public LayerMask PlayerLayerMask;
    public LayerMask PlatformsLayerMask;

    
    [Header("WallSlide Settings")]
    public LayerMask OnWallColliderLayerMask;
    public StateGuard WallSlideGuard;
    public StateGuard WallSlideFromGroundTransitionGuard;

    public float SlideClimbMovementTreshold = 0.1f;

    public float SlideClimbTopSpeed = 10.0f;
    public float SlideClimbTimeToMaxSpeed = 1.0f;
    public float SlideClimbTimeToStopFromMaxSpeed = 0.3f;

    public float SlideDownTopSpeed = 10.0f;
    public float SlideDownTimeToMaxSpeed = 1.0f;
    public float SlideDownTimeToStopFromMaxSpeed = 0.3f;

    [Header("Audio FX Settings")]
    public AudioClip JumpFX;
    public AudioClip LandFX, CrashFX, WalkFX, FlyFX, ChompItemFX, DropItemsFX, WallClimbFX, WallSlideDownFX, ScaredFX;

    [Header("Audio Music Settings")]
    public AudioClip LoungeIntro;
    public AudioClip LoungeLooping, AdventureIntro, AdventureLooping, DangerIntro, DangerLooping;
    public AudioClip Intermezzo;
}