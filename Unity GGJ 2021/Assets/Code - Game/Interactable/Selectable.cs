using UnityEngine;
using UnityEngine.Events;

namespace Interactable
{
    class Selectable : MonoBehaviour
    {
        private bool _selected;

        public bool selected
        {
            get
            {
                return _selected;
            }
            private set
            {
                _selected = value;
            }
        }

        public UnityEvent<bool> onChangeSelected;
        public UnityEvent onDeselected;
        public UnityEvent onSelected;

        public void SetSelected(bool active)
        {
            if (selected == active)
            {
                return;
            }

            // Store new state.
            _selected = active;

            // Dispatch update.
            onChangeSelected.Invoke(_selected);
            if (_selected)
            {
                onSelected.Invoke();
            }
            else
            {
                onDeselected.Invoke();
            }
        }
    }
}