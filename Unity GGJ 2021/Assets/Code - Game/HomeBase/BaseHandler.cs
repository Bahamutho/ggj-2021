using Interactable;

using System;

using UnityEngine;

namespace Homebase
{
    class BaseHandler : MonoBehaviour
    {
        [SerializeField]
        protected RangeHandler rangeHandler;

        protected void Awake()
        {
            if (!rangeHandler)
            {
                Debug.LogWarning("No RangeHandler found!");
            }
        }

        protected void OnEnable()
        {
            rangeHandler.onEnter.AddListener(PlayerEnteredRange);
        }

        protected void OnDisable()
        {
            rangeHandler.onEnter.AddListener(PlayerEnteredRange);
        }

        protected void PlayerEnteredRange(Collider2D playerCollider)
        {
            InventoryController inventoryController = playerCollider.GetComponent<InventoryController>();
            if (!inventoryController)
            {
                return;
            }

            inventoryController.onDequeue.AddListener(OnItemDropped);
            inventoryController.Empty();
            inventoryController.onDequeue.RemoveListener(OnItemDropped);
        }

        private void OnItemDropped(GameObject itemObject)
        {
            Rigidbody2D itemRigidbody = itemObject.GetComponent<Rigidbody2D>();
            Item item = itemObject.GetComponent<Item>();
        }
    }
}