using System;

using UnityEngine;
using UnityEngine.Events;
class SignalEmitter : MonoBehaviour
{
    public UnityEvent onEnable;
    public UnityEvent onDisable;

    protected void OnEnable()
    {
        onEnable.Invoke();
    }

    protected void OnDisable()
    {
        onDisable.Invoke();
    }
}
