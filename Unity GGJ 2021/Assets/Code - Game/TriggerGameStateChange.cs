using System;
using Sirenix.OdinInspector;
using UnityEngine;

public class TriggerGameStateChange : MonoBehaviour
{
    public LayerMask ConditionalLayerMask;
    public GlobalGameStateEnum NewState = GlobalGameStateEnum.Adventure;
    private GlobalGameState globalGameState;

    private void Awake()
    {
        globalGameState = GlobalGameState.Instance;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (MatchesLayerMask(other))
        {
            globalGameState.ChangeGameState(NewState);
        }
    }
    
    private bool MatchesLayerMask(Collider2D other)
    {
        return (ConditionalLayerMask & (1 << other.gameObject.layer)) > 0;
    }


}
