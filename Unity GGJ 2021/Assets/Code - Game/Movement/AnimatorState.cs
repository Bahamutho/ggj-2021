using UnityEngine;

class AnimatorState : MonoBehaviour
{
    protected Animator animator;
    protected InventoryController inventoryController;
    protected GroundedJumpController jumpController;
    protected CharacterState state;

    protected void Awake()
    {
        animator = GetComponent<Animator>();
        inventoryController = GetComponent<InventoryController>();
        jumpController = GetComponent<GroundedJumpController>();
        state = GetComponent<CharacterState>();
    }

    protected void OnEnable()
    {
        state.Burping.OnStart.AddListener(TriggerSpitting);
        state.Chomping.OnStart.AddListener(TriggerEating);
        state.Jumping.OnStart.AddListener(TriggerJump);
        state.Scared.OnStart.AddListener(Scared);
    }

    private void Scared()
    {
        animator.SetTrigger("ScaredTrigger");
    }

    protected void OnDisable()
    {
        state.Burping.OnStart.RemoveListener(TriggerSpitting);
        state.Chomping.OnStart.RemoveListener(TriggerEating);
        state.Jumping.OnStart.RemoveListener(TriggerJump);
    }

    protected void TriggerEating()
    {
        animator.SetTrigger("EatingTrigger");
    }

    protected void TriggerSpitting()
    {
        animator.SetTrigger("SpittingTrigger");
    }

    protected void TriggerJump()
    {
        animator.SetTrigger("JumpingTrigger");
    }

    protected void Update()
    {
        animator.SetBool("GroundedState", state.Grounded.Currently);
        animator.SetBool("FallingState", state.Falling.Currently);
        animator.SetBool("AirbornState", state.Airborn.Currently);
        animator.SetBool("JumpingState", state.Jumping.Currently);
        animator.SetBool("WalkingState", state.Walking.Currently);
        animator.SetBool("OnWallState", state.OnWall.Currently);
        animator.SetBool("OnLeftWallState", state.OnLeftWall.Currently);
        animator.SetBool("OnRightWallState", state.OnRightWall.Currently);
        animator.SetBool("ScaredState", state.Scared.Currently);
        animator.SetBool("WallSlidingState", state.WallSliding.Currently);
        animator.SetBool("WallClimbingState", state.WallClimbing.Currently);
        animator.SetBool("WallSlidingDownState", state.WallSlidingDown.Currently);
        animator.SetBool("WallClampingState", state.WallClamping.Currently);
    }
}