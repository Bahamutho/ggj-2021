using Interactable;

using Sirenix.OdinInspector;

using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CharacterState))]
class InteractionController : MonoBehaviour
{
    protected CharacterState state;
    [SerializeField, HideInInspector]
    private ObjectQueue objectQueue;
    [SerializeField]
    private Collider2D interactionCollider2D;

    private Rigidbody2D rb;
    
    protected void Awake()
    {
        state = GetComponent<CharacterState>();
        rb = GetComponent<Rigidbody2D>();
        
        // Start yelling if there is no object queue.
        if (!objectQueue)
        {
            objectQueue = GetComponent<ObjectQueue>();
        }
    }
    
    private void OnTriggerEnter2D(Collider2D otherCollider)
    {
        Chomp(otherCollider);
    }

    private void Chomp(Collider2D otherCollider)
    {
        // Cannot pickup when scared.
        if (state.Scared.Currently)
        {
            return;
        }

        if (!otherCollider.IsTouching(interactionCollider2D))
        {
            return;
        }
        
        Item item = otherCollider.GetComponent<Item>();
        if (item != null)
        {
            objectQueue.Enqueue(otherCollider.gameObject);
            state.Chomping.Start();
            state.Interacting.Start();
            StartCoroutine(StopAfterX());
            rb.velocity = Vector2.zero;
        }
    }

    private IEnumerator StopAfterX()
    {
        yield return new WaitForSeconds(1.25f);
        state.Chomping.Stop();
        state.Interacting.Stop();
    }
}