﻿using UnityEngine;

public class GroundedController : MonoBehaviour
{
    private GameSettings settings;
    private Rigidbody2D rb;
    private CharacterState state;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        settings = SettingsHolder.Instance.settings;
        state = GetComponent<CharacterState>();
    }

    void FixedUpdate()
    {
        if (state.Grounded.Currently)
        {
            if (!state.Grounded.Previously) // Wasn't grounded before
            {
                rb.gravityScale = settings.WalkingGravity;
            }
        }
    }
}