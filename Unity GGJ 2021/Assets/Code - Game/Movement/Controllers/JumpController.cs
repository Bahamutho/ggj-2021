using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(JumpTokens))]
public abstract class JumpController : MonoBehaviour
{
    protected GameSettings settings;
    protected Rigidbody2D rb;
    protected CharacterState state;
    protected JumpTokens jumpTokens;
    
    [ReadOnly, ShowInInspector]
    private float launchHeight = -1f;

    protected bool jumpEndedPrematurely = false;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        settings = SettingsHolder.Instance.settings;
        state = GetComponent<CharacterState>();
        jumpTokens = GetComponent<JumpTokens>();
    }

    protected void StartJumpBase(float timeToApex, float maxJumpHeight)
    {
        state.Jumping.Start();

        float gravity = 2 * maxJumpHeight / (timeToApex * timeToApex);
        rb.gravityScale = GravityHelper.GravityScale(gravity);
        
        float velocity = gravity * timeToApex;
        rb.velocity = new Vector2(rb.velocity.x, velocity);

        launchHeight = rb.position.y;
    }
    
    protected void StartJumpDirectional(float timeToApex, float maxJumpHeight, Vector2 dir)
    {
        state.Jumping.Start();

        float gravity = 2 * maxJumpHeight / (timeToApex * timeToApex);
        rb.gravityScale = GravityHelper.GravityScale(gravity);
        
        float velocity = gravity * timeToApex;
        rb.velocity = dir.normalized * velocity;

        launchHeight = rb.position.y;
    }


    public void EndJump()
    {
        if (!state.Jumping.Currently)
        {
            jumpEndedPrematurely = true;
            return;
        }

        state.Jumping.Stop();
        jumpEndedPrematurely = false;

        bool alreadyFalling = rb.velocity.y <= 0;
        if (alreadyFalling)
            return;

        bool higherThanMinJumpHeight = rb.position.y > launchHeight + settings.MinJumpHeight;
        if (higherThanMinJumpHeight)
        {
            rb.gravityScale = settings.MinJumpSlowDownGravityScale;
        }
        else
        {
            // Did not reach minApex yet
            float timeToReachApex = settings.TimeToMinJumpApex;
            var gravity = rb.velocity.y / timeToReachApex;
            rb.gravityScale = GravityHelper.GravityScale(gravity);
        }
    }
}
