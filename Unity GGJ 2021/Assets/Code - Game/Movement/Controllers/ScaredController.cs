using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CharacterState))]
class ScaredController : MonoBehaviour
{
    protected CharacterState state;
    [SerializeField]
    protected InventoryController inventoryController;

    protected void Awake()
    {
        state = GetComponent<CharacterState>();
        if (!inventoryController)
        {
            inventoryController = GetComponent<InventoryController>();
        }
        state.Scared.OnStart.AddListener(ScaredReaction);
    }

    private void ScaredReaction()
    {
        StartCoroutine(ScaredCoroutine());
        
    }

    private IEnumerator ScaredCoroutine()
    {
        yield return new WaitForSeconds(0.3f);
        
        inventoryController.Empty();
    }
}