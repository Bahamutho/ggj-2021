using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    private GameSettings settings;
    private float horInput = 0f;
    private Rigidbody2D rb;
    private CharacterState state;

    void Start()
    {
        settings = SettingsHolder.Instance.settings;
        rb = GetComponent<Rigidbody2D>();
        state = GetComponent<CharacterState>();
    }

    public void SetInput(float horizontalInput)
    {
        horInput = horizontalInput;
    }

    void FixedUpdate()
    {
        if (settings.MovementGuard.IsBlockedByGuard(state))
        {
            return;
        }

        float newX = MovementHelper.CalculateNewVelocityAfterAcceleration(
            settings.WalkingMaxSpeed,
            settings.TimeToWalkingMaxSpeed,
            settings.TimeToWalkingDeAccel, 
            horInput, 
            rb.velocity.x,
            settings.GroundedMovementTreshold,
            settings.XDeadZone);

        rb.velocity = new Vector2(newX, rb.velocity.y);
    }
}