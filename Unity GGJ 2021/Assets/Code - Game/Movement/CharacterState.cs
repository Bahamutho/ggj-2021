using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public class CharacterState : MonoBehaviour
{
    public StateData Grounded = new StateData();
    public StateData Falling = new StateData();
    public StateData Airborn = new StateData();
    public StateData Jumping = new StateData();
    public StateData Walking = new StateData();
    public StateData Chomping = new StateData();
    public StateData Burping = new StateData();
    public StateData OnWall = new StateData();
    public StateData OnLeftWall = new StateData();
    public StateData OnRightWall = new StateData();
    public StateData Scared = new StateData();
    public StateData WallSliding = new StateData();
    public StateData WallClimbing = new StateData();
    public StateData WallSlidingDown = new StateData();
    public StateData WallClamping = new StateData();
    public StateData LastGrounded = new StateData();
    public StateData Interacting = new StateData();
    public StateData BelowLedge = new StateData();
    public StateData BelowLeftLedge = new StateData();
    public StateData BelowRightLedge = new StateData();

    void Update()
    {
        Grounded.Update(Time.deltaTime);
        Falling.Update(Time.deltaTime);
        Airborn.Update(Time.deltaTime);
        Jumping.Update(Time.deltaTime);
        Walking.Update(Time.deltaTime);
        Chomping.Update(Time.deltaTime);
        Burping.Update(Time.deltaTime);
        Interacting.Update(Time.deltaTime);
        OnWall.Update(Time.deltaTime);
        OnLeftWall.Update(Time.deltaTime);
        OnRightWall.Update(Time.deltaTime);
        Scared.Update(Time.deltaTime);
        WallSliding.Update(Time.deltaTime);
        WallClimbing.Update(Time.deltaTime);
        WallSlidingDown.Update(Time.deltaTime);
        WallClamping.Update(Time.deltaTime);
        LastGrounded.Update(Time.deltaTime);
        BelowLedge.Update(Time.deltaTime);
        BelowLeftLedge.Update(Time.deltaTime);
        BelowRightLedge.Update(Time.deltaTime);
    }
}

[System.Serializable, InlineProperty()]
public class StateData
{
    public bool Currently => currently;

    [ShowInInspectorAttribute, HorizontalGroup(), LabelWidth(20f), ReadOnly]
    private bool currently = false;

    public bool Previously => previously;

    [ShowInInspectorAttribute, HorizontalGroup(), LabelWidth(20f), ReadOnly]
    private bool previously = false;

    public float TimeOn => timeOn;

    [ShowInInspectorAttribute, HorizontalGroup(), LabelWidth(20f), ReadOnly]
    private float timeOn = 0f;

    [HideInInspector] public UnityEvent OnStart = new UnityEvent();
    [HideInInspector] public UnityEvent OnStop = new UnityEvent();
    [HideInInspector] public UnityEvent OnUpdate = new UnityEvent();

    public void Start()
    {
        if (currently)
            return;

        currently = true;
        timeOn = 0f;

        OnStart.Invoke();
    }

    public void Stop()
    {
        currently = false;
        timeOn = 0f;

        OnStop.Invoke();
    }

    public void Update(float deltaTime)
    {
        if (currently) timeOn += deltaTime;

        previously = currently;

        OnUpdate.Invoke();
    }
}