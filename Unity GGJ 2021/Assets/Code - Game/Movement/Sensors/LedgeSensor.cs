﻿using UnityEngine;

    public class LedgeSensor : MonoBehaviour
    {
        public WallCollisionDetectors Left, Right;
        
        private CharacterState state;
        
        void Start()
        {
            state = GetComponent<CharacterState>();
        }
    
        void FixedUpdate()
        {
            var belowLeft = Left.BelowLedge();
            var belowRight = Right.BelowLedge();
            
            if (belowLeft || belowRight)
                state.BelowLedge.Start();
            else
                state.BelowLedge.Stop();

            if (belowLeft)
                state.BelowLeftLedge.Start();
            else
                state.BelowLeftLedge.Stop();
            
            if (belowRight)
                state.BelowRightLedge.Start();
            else
                state.BelowRightLedge.Stop();
        }
    }
