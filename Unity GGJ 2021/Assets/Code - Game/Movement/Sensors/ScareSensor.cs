using Entity.Signals;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CharacterState))]
class ScareSensor : StateSensor
{
    private CharacterState state;
    private string scaryTag = "Scary";
    private Rigidbody2D rb;
    private GameSettings settings => SettingsHolder.Instance.settings;

    protected void Awake()
    {
        state = GetComponent<CharacterState>();
        rb = GetComponent<Rigidbody2D>();
    }

    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        if (!otherCollider.CompareTag(scaryTag))
            return;

        state.Scared.Start();
        state.Interacting.Start();

        jumpAway(otherCollider);

        StartCoroutine(StopAfterX());
    }

    private void jumpAway(Collider2D otherCollider)
    {
        var direction = (rb.transform.position - otherCollider.transform.position).normalized;
        float jumpX = Mathf.Sign(direction.x) * settings.ScaredJumpX;
        rb.velocity = new Vector2(jumpX, settings.ScaredJumpY);
    }

    private IEnumerator StopAfterX()
    {
        yield return new WaitForSeconds(1.0f);
        state.Scared.Stop();
        state.Interacting.Stop();
    }
}