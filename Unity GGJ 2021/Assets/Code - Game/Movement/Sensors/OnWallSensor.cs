﻿using UnityEngine;

namespace DefaultNamespace
{
    public class OnWallSensor : StateSensor
    {
        private GameSettings settings;
        private CharacterState state;
        public Collider2D leftCollider;
        public Collider2D rightCollider;
    
        void Start()
        {
            settings = SettingsHolder.Instance.settings;
            state = GetComponent<CharacterState>();
        }
    
        void FixedUpdate()
        {
            var left = leftCollider.IsTouchingLayers(settings.OnWallColliderLayerMask);
            var right = rightCollider.IsTouchingLayers(settings.OnWallColliderLayerMask);
            
            if(left) 
                state.OnLeftWall.Start();
            else 
                state.OnLeftWall.Stop();
            
            if(right) 
                state.OnRightWall.Start();
            else 
                state.OnRightWall.Stop();

            if (left || right)
                state.OnWall.Start();
            else
                state.OnWall.Stop();
        }
    }
}