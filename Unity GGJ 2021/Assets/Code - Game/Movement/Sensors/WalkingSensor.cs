using System;

using UnityEngine;

[RequireComponent(typeof(CharacterState))]
class WalkingSensor : CombinedStateSensor
{
    protected new Rigidbody2D rigidbody2D;
    protected GameSettings settings;
    protected CharacterState state;

    protected void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        settings = SettingsHolder.Instance.settings;
        state = GetComponent<CharacterState>();
    }

    protected void FixedUpdate()
    {
        if (!settings.WalkingGuard.IsBlockedByGuard(state) && Mathf.Abs(rigidbody2D.velocity.x) > settings.WalkingSensingThreshold)
        {
            state.Walking.Start();
        }
        else
        {
            state.Walking.Stop();
        }
    }
}