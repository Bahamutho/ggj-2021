using Interactable;

using System.Collections;

using UnityEngine;

public class DropItemsAtHome : MonoBehaviour
{
    private CharacterState state;
    private InventoryController inventoryController;
    private Transform HomeTarget;

    protected void Awake()
    {
        state = GetComponentInParent<CharacterState>();
        inventoryController = GetComponentInParent<InventoryController>();

        HomeTarget = GameObject.FindWithTag("Home").transform;
    }

    protected void OnTriggerEnter2D(Collider2D otherCollider)
    {
        var home = ((1<<otherCollider.gameObject.layer) & SettingsHolder.Instance.settings.HomeColliderLayerMask) != 0;

        if (!home)
            return;

        if (!inventoryController.HasItems())
            return;

        inventoryController.onDequeue.AddListener(OnDropItem);

        state.Chomping.Start();
        state.Interacting.Start();

        inventoryController.Empty();

        StartCoroutine(StopAfterX());

        inventoryController.onDequeue.RemoveListener(OnDropItem);
    }

    private IEnumerator StopAfterX()
    {
        yield return new WaitForSeconds(1.0f);
        state.Chomping.Stop();
        state.Interacting.Stop();
    }

    private void OnDropItem(GameObject itemObject)
    {
        var toTarget = itemObject.GetComponentInChildren<MoveRigidbody2DToTarget>();
        toTarget.gameObject.layer = LayerMask.NameToLayer("ItemInHomeBase");

        if (HomeTarget != null)
        {
            toTarget.Target = HomeTarget;
        }
        
        Item item = itemObject.GetComponent<Item>();
        if (item)
        {
            Destroy(item);
        }

        itemObject.GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-5f,5f);
        itemObject.GetComponent<Collider2D>().isTrigger = false;

        StartCoroutine(DisableMoveRBToTargetAfterX(toTarget));
    }

    private IEnumerator DisableMoveRBToTargetAfterX(MoveRigidbody2DToTarget moveRigidbody2DToTarget)
    {
        yield return new WaitForSeconds(1.5f);
        // moveRigidbody2DToTarget.enabled = false;
    }
}