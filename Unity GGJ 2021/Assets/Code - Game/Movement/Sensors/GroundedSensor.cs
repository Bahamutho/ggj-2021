using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class GroundedSensor : StateSensor
{
    private GameSettings settings;
    private CharacterState state;
    public Collider2D groundedCollider;
    private Rigidbody2D rb;
    
    void Start()
    {
        settings = SettingsHolder.Instance.settings;
        state = GetComponent<CharacterState>();
        if (groundedCollider == null)
        {
            groundedCollider = GetComponent<Collider2D>();
        }

        rb = GetComponent<Rigidbody2D>();
    }
    
    void FixedUpdate()
    {
        var isTouchingLayers = groundedCollider.IsTouchingLayers(settings.GroundedColliderLayerMask);
        var belowUpwardsVelocityTreshold = rb.velocity.y <= settings.GroundedUpwardVelocityTreshold;
        if (isTouchingLayers && belowUpwardsVelocityTreshold)
        {
            state.Grounded.Start();
            state.LastGrounded.Stop();
        }
        else
        {
            state.Grounded.Stop();
            state.LastGrounded.Start();
        }
    }
}
