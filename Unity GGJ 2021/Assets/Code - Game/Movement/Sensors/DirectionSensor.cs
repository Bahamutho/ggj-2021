using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

class DirectionSensor : MonoBehaviour
{
    protected Direction direction;
    protected new SpriteRenderer renderer;
    protected new Rigidbody2D rigidbody2D;
    protected GameSettings settings;
    protected CharacterState state;

    [SerializeField]
    protected List<OffsetFlipper> offsetFlippers = new List<OffsetFlipper>();

    public UnityEvent<Direction> onChange;
    public UnityEvent onLeft;
    public UnityEvent onRight;

    protected void Awake()
    {
        renderer = GetComponent<SpriteRenderer>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        settings = SettingsHolder.Instance.settings;
        state = GetComponent<CharacterState>();
    }

    protected void FixedUpdate()
    {
        if (settings.FlipDirectionGuard.IsBlockedByGuard(state))
        {
            return;
        }
        
        Direction newDirection;

        if (state.OnLeftWall.Currently)
        {
            // If both then keep current.
            if (state.OnRightWall.Currently)
            {
                return;
            }

            newDirection = Direction.Left;
        }
        else if (state.OnRightWall.Currently)
        {
            newDirection = Direction.Right;
        }
        else
        {
            // Keep previous direction if not enough velocity.
            if (Mathf.Abs(rigidbody2D.velocity.x) <= settings.WalkingSensingThreshold)
            {
                return;
            }

            newDirection = rigidbody2D.velocity.x < 0f ? Direction.Left : Direction.Right;
        }

        if (direction == newDirection)
        {
            return;
        }

        direction = newDirection;

        renderer.flipX = direction == Direction.Right;
        foreach (OffsetFlipper flipper in offsetFlippers)
        {
            flipper.Set(direction);
        }

        onChange.Invoke(direction);

        if (direction == Direction.Left)
        {
            onLeft.Invoke();
        }
        else
        {
            onRight.Invoke();
        }
    }
}