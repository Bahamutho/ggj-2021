using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Audio;
using System.Linq;

public class AudioManager : SingletonMonoBehavior<AudioManager>
{
    public AudioSource Source;
    public AudioSource Source2;
    public AudioSource SourceFx;

    public AudioListener Listener;

    public AudioMixer Mixer;
    public AudioMixerGroup MixerGroup;

    [OnValueChanged("SetVolume")] [Range(0.001f, 1)]
    public float Volume = 1.0f;

    [ReadOnly, SerializeField] private float decibel = 0f;

    double clipDuration => (Source.clip != null) ? (double) Source.clip.samples / Source.clip.frequency : 0.0;
    public string ClipProgress => $"[{sourcePlaying?.time:F2}/{clipDuration:F2}]";

    private AudioSource sourcePlaying;

    void Start()
    {
        if (Source == null)
            Source = gameObject.AddComponent<AudioSource>();
        if (Listener == null)
            Listener = gameObject.AddComponent<AudioListener>();
    }

    public void Stop()
    {
        // Change this to fade out?
        Source.Stop();
        Source2.Stop();
    }

    [Button]
    public void PlayClip(AudioClip clip, Action onFinished, float invokeOnFinishedFasterInSeconds, float fadeDuration = 0.2f)
    {
        StopAllCoroutines();

        if (clip == null)
        {
            onFinished.Invoke();
            return;
        }

        // TODO crossfade if still playing
        if (sourcePlaying != null && sourcePlaying.isPlaying)
            CrossFade(clip, fadeDuration);
        else
        {
            sourcePlaying = Source;
            sourcePlaying.clip = clip;
            sourcePlaying.volume = 1;
            sourcePlaying.Play();
        }

        StartCoroutine(WaitTillFinished(onFinished, invokeOnFinishedFasterInSeconds));
    }

    private void CrossFade(AudioClip clip, float fadeDuration)
    {
        StartCoroutine(CrossFadeCoroutine(clip, fadeDuration));
    }

    private IEnumerator CrossFadeCoroutine(AudioClip clip, float fadeDuration)
    {
        AudioSource oldSource = sourcePlaying;
        AudioSource newSource = Source == sourcePlaying ? Source2 : Source;

        float timeStarted = Time.timeSinceLevelLoad;
        float progress = 0;

        newSource.clip = clip;
        newSource.volume = 0;
        newSource.Play();

        while (progress < fadeDuration)
        {
            progress = Time.timeSinceLevelLoad - timeStarted;

            float progressFraction = progress / fadeDuration;

            oldSource.volume = Mathf.Lerp(1, 0, progressFraction);
            newSource.volume = Mathf.Lerp(0, 1, progressFraction);

            Debug.Log(
                $"XFade {progress} {progressFraction} {fadeDuration} {Mathf.Lerp(1, 0, progress)} {Mathf.Lerp(0, 1, progress)}");

            yield return null;
        }

        oldSource.volume = 0;
        oldSource.Stop();
        newSource.volume = 1;
        sourcePlaying = newSource;
    }

    private IEnumerator WaitTillFinished(Action onFinished, float invokeOnFinishedFasterInSeconds)
    {
        Debug.Log($" {Source.clip.name} {clipDuration:F2} {sourcePlaying.isPlaying}");

        while (sourcePlaying.isPlaying)
        {
            if (sourcePlaying.time + invokeOnFinishedFasterInSeconds > clipDuration)
            {
                break;
            }

            yield return null;
        }

        yield return new WaitWhile(() => sourcePlaying.isPlaying);

        onFinished.Invoke();
    }

    [Button]
    public void PlayEffect(AudioClip fx)
    {
        Debug.Log($"Playing FX {fx}");
        if (fx == null) return;

        SourceFx.PlayOneShot(fx);
    }

    public AudioSource PlayLoopingEffect(AudioClip fx)
    {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.volume = 1;
        audioSource.outputAudioMixerGroup = MixerGroup;
        audioSource.loop = true;
        audioSource.clip = fx;
        audioSource.Play();

        return audioSource;
    }

    
    
    public void StopLoopingEffectAndDestroyAudioSource(AudioClip fx)
    {
        AudioSource[] audioSources = GetComponents<AudioSource>();
        var audioSource = audioSources.First(s => s.clip == fx);
        if (audioSource == null)
            return;

        StopLoopingEffectAndDestroyAudioSource(audioSource);
    }
    
    public void StopLoopingEffectAndDestroyAudioSource(AudioSource audioSource)
    {
        if (audioSource == null)
            return;
        
        audioSource.Stop();
        Destroy(audioSource);
    }

    [Button]
    public void SetVolume(float settingsVolumeLevel)
    {
        Volume = Mathf.Clamp01(settingsVolumeLevel);

        if (Mixer != null)
        {
            decibel = Mathf.Log10(Volume) * 20;

            var result = Mixer.SetFloat("MasterVolume", decibel);
            if (!result)
            {
                Debug.LogError($"Cannot set volume of {Mixer}");
            }
        }
    }

    [Button]
    public void PlayClipsLoopWithIntro(AudioClip intro, AudioClip loopClip)
    {
        PlayClip(intro, () => PlayClipOnLoop(loopClip), 0f, 2.0f);
    }

    public void PlayClipOnLoop(AudioClip clip)
    {
        PlayClip(clip, () => PlayClipOnLoop(clip), 0f, 0.2f);
    }
}