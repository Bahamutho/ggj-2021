﻿using System;
using UnityEngine;
using UnityEngine.Events;


public class AudioFXManager : MonoBehaviour
{
    
    private CharacterState state;
    private GameSettings settings => SettingsHolder.Instance.settings;
    
    private void Awake()
    {
        state = GetComponent<CharacterState>();
        addListenerFX(state.Grounded.OnStart, settings.LandFX);
        addListenerFX(state.Jumping.OnStart, settings.JumpFX);
        addLoopingListenerFX(state.Falling, settings.FlyFX);
        addLoopingListenerFX(state.WallClimbing, settings.WallClimbFX);
        addLoopingListenerFX(state.WallSlidingDown, settings.WallSlideDownFX);
        addLoopingListenerFX(state.Walking, settings.WalkFX);
        addListenerFX(state.Scared.OnStart, settings.ScaredFX);
        addListenerFX(state.Chomping.OnStart, settings.ChompItemFX);
    }

    private void addLoopingListenerFX(StateData stateData, AudioClip fx)
    {
        AudioSource source = new AudioSource();
        stateData.OnStart.AddListener(() => source = AudioManager.Instance.PlayLoopingEffect(fx));
        stateData.OnStop.AddListener(() => AudioManager.Instance.StopLoopingEffectAndDestroyAudioSource(source));
    }

    private void addListenerFX(UnityEvent @event, AudioClip clip)
    {
        @event.AddListener(() => AudioManager.Instance.PlayEffect(clip));
    }
}